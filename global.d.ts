/// <reference types="vite/client" />

import { Axios } from 'axios';

declare global {
  interface Window {
    axios: Axios
  }
}

window.axios = window.axios || {}

export {}
