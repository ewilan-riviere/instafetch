<?php

use function Pest\Laravel\get;

it('test_the_application_returns_a_successful_response', function () {
    $response = get('/');

    $response->assertStatus(200);
});
