<div
  class="min-h-screen bg-gray-900 py-10 sm:py-16"
  x-data="{
      seconds: 0,
      secondsString: '',
      init() {
          setInterval(() => {
              $wire.fetchData();
          }, 3000);
  
          $watch('seconds', (value) => {
              if (value === 14) {
                  this.secondsString = 'Profile is fetched! Wait a moment...';
              }
          });
  
          setInterval(() => {
              if (this.seconds < 14) {
                  this.seconds++;
                  this.secondsString = `${this.seconds} seconds elapsed`;
              }
          }, 1000);
      }
  }"
>
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    @if (!$loaded)
      <div class="mx-auto w-max text-center">
        <div class="flex items-center space-x-3">
          <x-loading />
          <div>Wait some seconds, Instafetch is fetching profile...</div>
        </div>
        <div>Typically, it takes 14 seconds...</div>
        <div>
          <span x-text="secondsString"></span>
        </div>
      </div>
    @else
      <div class="mx-auto max-w-2xl">
        <x-header :account="$account">
          <div class="flex items-center space-x-5">
            <button
              class="rounded-md bg-blue-500 px-5 py-1 hover:bg-blue-600"
              type="button"
              wire:click="refresh"
              wire:loading.attr="disabled"
              wire:target="refresh"
            >
              Refresh
            </button>
            @isset($profile->last_fetched)
              <div class="text-xs">
                Last update: {{ date_format($profile->last_fetched, 'Y/m/d H:i:s') }}
              </div>
            @endisset
          </div>
          {{-- <button
          class="rounded-md bg-blue-500 px-5 py-1 hover:bg-blue-600"
          type="button"
          wire:click="refresh"
          wire:loading.attr="disabled"
          wire:target="refresh"
        >
          Reload profile
        </button> --}}
        </x-header>
      </div>
      <div
        class="mx-auto mt-16 grid max-w-2xl auto-rows-fr grid-cols-1 gap-8 sm:mt-20 lg:mx-0 lg:max-w-none lg:grid-cols-3"
      >
        @foreach ($posts as $post)
          <x-post :post="$post" />
        @endforeach
      </div>
    @endif
  </div>
</div>
