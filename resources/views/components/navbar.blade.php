<nav class="bg-gray-800">
  <div class="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
    <div class="relative flex h-16 items-center justify-between">
      <div class="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
        <div class="flex flex-shrink-0 items-center">
          <img
            class="h-8 w-auto"
            src="/instafetch.svg"
            alt="Your Company"
          >
        </div>
      </div>
      <a
        class="flex h-8 w-8 text-white"
        href="https://gitlab.com/ewilan-riviere/instafetch"
        target="_blank"
        rel="noopener noreferrer"
      >
        <svg
          class="m-auto"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 25 24"
          height="24"
          width="25"
        >
          <path
            class="tanuki-shape tanuki"
            fill="#E24329"
            d="m24.507 9.5-.034-.09L21.082.562a.896.896 0 0 0-1.694.091l-2.29 7.01H7.825L5.535.653a.898.898 0 0 0-1.694-.09L.451 9.411.416 9.5a6.297 6.297 0 0 0 2.09 7.278l.012.01.03.022 5.16 3.867 2.56 1.935 1.554 1.176a1.051 1.051 0 0 0 1.268 0l1.555-1.176 2.56-1.935 5.197-3.89.014-.01A6.297 6.297 0 0 0 24.507 9.5Z"
          ></path>
          <path
            class="tanuki-shape right-cheek"
            fill="#FC6D26"
            d="m24.507 9.5-.034-.09a11.44 11.44 0 0 0-4.56 2.051l-7.447 5.632 4.742 3.584 5.197-3.89.014-.01A6.297 6.297 0 0 0 24.507 9.5Z"
          ></path>
          <path
            class="tanuki-shape chin"
            fill="#FCA326"
            d="m7.707 20.677 2.56 1.935 1.555 1.176a1.051 1.051 0 0 0 1.268 0l1.555-1.176 2.56-1.935-4.743-3.584-4.755 3.584Z"
          ></path>
          <path
            class="tanuki-shape left-cheek"
            fill="#FC6D26"
            d="M5.01 11.461a11.43 11.43 0 0 0-4.56-2.05L.416 9.5a6.297 6.297 0 0 0 2.09 7.278l.012.01.03.022 5.16 3.867 4.745-3.584-7.444-5.632Z"
          ></path>
        </svg>
      </a>
    </div>
  </div>
</nav>
