<div class="flex items-center gap-x-20">
  <img
    class="h-36 w-36 rounded-full object-cover text-lg"
    src="{{ $account->avatar }}"
    alt="{{ $account->name }}"
  >
  <div class="max-w-lg space-y-3">
    <div class="flex items-center space-x-6">
      <a
        class="underline"
        href="{{ $account->url }}"
        target="_blank"
        rel="noopener noreferrer"
      >
        <h1 class="text-lg font-medium">
          {{ $account->name }}
        </h1>
      </a>
      <div>
        {{ $slot }}
      </div>
    </div>
    <ul class="flex justify-between">
      <li>
        {{ $account->current_posts }} posts
      </li>
      <li>
        {{ $account->current_followers }} followers
      </li>
      <li>
        {{ $account->current_following }} following
      </li>
    </ul>
    <p class="text-sm font-medium text-gray-300">
      {{ $account->description }}
    </p>
  </div>
</div>
