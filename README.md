# Instafetch

[![composer][composer-src]][composer-href]
[![php][php-src]][php-href]
[![laravel][laravel-src]][laravel-href]

[![pnpm][pnpm-src]][pnpm-href]
[![node][node-src]][node-href]

Instagram feed fetch app.

## Requirements

-   You have to create an Instagram accout, don't use your personal account.
-   `symfony/panther` is used to fetch Instagram profile, you have to install some dependencies, see [here](https://github.com/symfony/panther) for more informations.
-   On this project, we use [pnpm](https://pnpm.io) to manage frontend dependencies, you can use [npm](https://www.npmjs.com) or [yarn](https://yarnpkg.com) if you want.

On macOS, you can install them with Homebrew

```bash
brew install chromedriver geckodriver
```

## Installation

Install dependencies

```bash
composer install
pnpm install
```

Create `.env` file

```bash
cp .env.example .env
```

Fill these fields in `.env` file with your Instagram account credentials, it's highly recommanded to create a new account for this purpose.

```bash
INSTAGRAM_ACCOUNT_USERNAME=
INSTAGRAM_ACCOUNT_PASSWORD=
```

You can choose another Instagram profile to fetch by changing this field in `.env` file

```bash
INSTAGRAM_PROFILE=arcaneshow
```

Generate application key

```bash
php artisan key:generate
```

Create storage link

```bash
php artisan storage:link
```

Create database into your prefered service and run database migrations

```bash
php artisan migrate:fresh
```

### Development

Run Vite, on watch mode

```bash
pnpm dev
```

Run queue worker

```bash
composer queue
```

Run development server

```bash
php artisan serve
```

## Usage

Instafetch allow you to fetch an instagram profile feed and to display it in application. When you, start server, you can just go to `http://localhost:8000`, a message will be displayed if you have no posts in database, fetching is automatic after some time.

### How it works

Instafetch use web scrapping to extract data from Instagram profiles.

### Limitations

Instagram API is not public and some limitations are applied to Instagram web scrapping:

-   You can't fetch more than 12 posts per profile.
-   **Web scrapping is not allowed by Instagram, so you can be blocked by Instagram if you use this application too much**.
-   A cache system is implemented to avoid fetching the same profile too much.

If Instagram block you, you can't do anything, you have to wait.

## Production

To have a up-to-date feed, you can use a cron job to fetch the profile every hour.

```bash
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```

## Commands

You can change instagram profile to fetch by running this command, of course you can change it into `.env` file but you will have to clear cache.

```bash
php artisan instafetch:profile <username>
```

After that, you can fetch the profile with this command

```bash
php artisan instafetch -r
```

### Examples

You can try these profiles

Do you like Jinx?

```bash
php artisan instafetch:profile arcaneshow
php artisan instafetch -r
```

Do you like Walter White?

```bash
php artisan instafetch:profile breakingbad
php artisan instafetch -r
```

Do you like Teal'c, the Goa'uld?

```bash
php artisan instafetch:profile stargate_sg1_
php artisan instafetch -r
```

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[composer-src]: https://img.shields.io/static/v1?label=Composer&message=v2.x&color=885630&style=flat-square&logo=composer&logoColor=ffffff
[composer-href]: https://getcomposer.org
[php-src]: https://img.shields.io/static/v1?label=PHP&message=v8.1&color=777bb4&style=flat-square&logo=php&logoColor=ffffff
[php-href]: https://www.php.net
[laravel-src]: https://img.shields.io/static/v1?label=Laravel&message=v10.x&color=ff2d20&style=flat-square&logo=laravel&logoColor=ffffff
[laravel-href]: https://laravel.com
[pnpm-src]: https://img.shields.io/static/v1?label=pnpm&message=v8.x&color=F69220&style=flat-square&logo=pnpm&logoColor=ffffff
[pnpm-href]: https://pnpm.io
[node-src]: https://img.shields.io/static/v1?label=Node.js&message=v18.x&color=339933&style=flat-square&logo=node.js&logoColor=ffffff
[node-href]: https://nodejs.org/en
