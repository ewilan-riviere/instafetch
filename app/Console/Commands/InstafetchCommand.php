<?php

namespace App\Console\Commands;

use App\Services\InstagramService;
use Illuminate\Console\Command;

class InstafetchCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instafetch
                            {--r|refresh : Refresh the cache before fetching}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute fetch on profile into config.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $refresh = $this->option('refresh');
        $profile = InstagramService::getProfile();

        if (! $profile) {
            $this->error('No profile found in config file, set `INSTAGRAM_PROFILE`.');

            return;
        }

        $this->info('Parsed profile: '.$profile->username);

        InstagramService::make($profile->username, $refresh);

        $this->info('Done!');
    }
}
