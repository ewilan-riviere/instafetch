<?php

namespace App\Console\Commands;

use App\Utils\Dotenv;
use Illuminate\Console\Command;

class InstafetchProfileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instafetch:profile {username : The username of the profile to fetch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the profile to fetch.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $username = $this->argument('username');

        Dotenv::addToDotEnv('INSTAGRAM_PROFILE', $username);
        config(['instagram.profile' => $username]);
        $profile = config('instagram.profile');

        $this->info("Username set successfully: {$profile}");
    }
}
