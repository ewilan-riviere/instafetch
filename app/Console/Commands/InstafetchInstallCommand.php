<?php

namespace App\Console\Commands;

use App\Utils\Dotenv;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\text;

class InstafetchInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instafetch:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Instafetch application';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Welcome on Instafetch!');
        $this->newLine();

        if (File::exists(base_path('.env'))) {
            $this->warn('The .env file already exists, it can contains important data.');
            $confirmed = confirm(
                label: 'Do you accept the delete it?',
                default: false,
            );
            if ($confirmed) {
                File::delete(base_path('.env'));
                $this->comment('The .env file has been deleted.');
            } else {
                $this->comment('The .env file has not been deleted.');
                $this->warn('Stop the installation.');

                return;
            }

        }

        $this->comment('Create the .env file...');
        File::copy(base_path('.env.example'), base_path('.env'));
        $this->newLine();

        // database user
        $this->info('Instafetch will use database to store data, can you insert the username?');
        $dbUser = text(
            label: 'Insert database username',
            placeholder: 'root',
            default: 'root',
            required: true
        );
        Dotenv::addToDotEnv('DB_USERNAME', $dbUser);
        $this->comment('Thanks, five steps left!');
        $this->newLine();

        // database password
        $this->info('And the password?');
        $dbPassword = text(
            label: 'Insert database password',
            placeholder: '',
            default: '',
        );
        Dotenv::addToDotEnv('DB_PASSWORD', $dbPassword);
        $this->comment('Thanks, four steps left!');
        $this->newLine();

        // database
        $this->info('And database name?');
        $database = text(
            label: 'Insert database name',
            placeholder: 'instafetch',
            default: 'instafetch',
            required: true
        );
        Dotenv::addToDotEnv('DB_DATABASE', $database);
        $this->warn('Now you have to create the database, can you do it?');
        $confirmed = confirm(
            label: 'Database is created?',
            default: false,
            required: true
        );
        if (! $confirmed) {
            $this->warn('Stop the installation.');

            return;
        }
        $this->comment('Thanks, three steps left!');
        $this->newLine();

        // instagram username
        $this->info('Instafetch will use an Instagram account, can you insert the username?');
        $username = text(
            label: 'What is your Instagram username?',
            required: true
        );
        Dotenv::addToDotEnv('INSTAGRAM_ACCOUNT_USERNAME', $username);
        $this->comment('Thanks, two steps left!');
        $this->newLine();

        // instagram password
        $this->info('Instafetch will use an Instagram account, can you insert the password?');
        $password = text(
            label: 'What is your Instagram password?',
            required: true
        );
        Dotenv::addToDotEnv('INSTAGRAM_ACCOUNT_PASSWORD', $password);
        $this->comment('Thanks, one step left!');
        $this->newLine();

        // instagram profile
        $this->info('Instafetch will use an Instagram profile, can you insert the profile?');
        $profile = text(
            label: 'What is the Instagram profile?',
            placeholder: 'arcaneshow',
            default: 'arcaneshow',
            required: true
        );
        Dotenv::addToDotEnv('INSTAGRAM_PROFILE', $profile);
        $this->comment("Thanks, it's finished!");
        $this->newLine();

        File::delete(base_path('bootstrap/cache/config.php'));
        $this->call('config:cache');
        $this->call('config:clear');
        $this->call('cache:clear');

        $this->comment('Generate the application key...');
        $this->call('key:generate');

        $this->comment('Generate storage...');
        $this->call('storage:link');

        $this->comment('Migrate database...');
        $this->call('migrate:fresh');

        $this->comment('Run pnpm...');
        exec('pnpm i');
        exec('pnpm build');

        $this->info('You can now start the application:');
        $this->comment('php artisan serve');
        $this->newLine();
        $this->info("Don't forget to run the queue worker:");
        $this->comment('composer queue');
        $this->newLine();
        $this->info('If you want to change front-end, you can run:');
        $this->comment('pnpm dev');
        $this->newLine();
    }
}
