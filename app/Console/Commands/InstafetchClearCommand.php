<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Post;
use App\Models\Profile;
use Illuminate\Console\Command;

class InstafetchClearCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instafetch:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the instafetch cache.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Clearing instafetch cache...');

        Profile::query()->delete();
        Account::query()->delete();
        Post::query()->delete();

        $this->info('Instafetch cache cleared!');
    }
}
