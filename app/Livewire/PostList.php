<?php

namespace App\Livewire;

use App\Jobs\InstafetchRefreshProcess;
use App\Models\Account;
use App\Models\Post;
use App\Models\Profile;
use Illuminate\Support\Collection;
use Livewire\Component;

class PostList extends Component
{
    public ?Profile $profile = null;

    public ?Account $account = null;

    /** @var Collection<int,Post>|null */
    public ?Collection $posts = null;

    public bool $loaded = false;

    public function refresh()
    {
        InstafetchRefreshProcess::dispatch();
    }

    public function fetchData()
    {
        $exists = Profile::query()->exists();
        if (! $exists) {
            return;
        }

        $profile = Profile::first();
        if ($profile) {
            /** @var Profile $profile */
            $this->profile = $profile;
        }

        $account = Account::first();
        if ($account) {
            /** @var Account $account */
            $account->load('posts');
            $posts = $account->posts;
            $this->account = $account;
            $this->posts = $posts;

            $this->loaded = true;
        }
    }

    public function mount()
    {
        $this->posts = collect();

        $this->profile = Profile::query()->first();
        if (! $this->profile) {
            InstafetchRefreshProcess::dispatch();
        }

        $this->account = Account::query()->first();
        if ($this->account) {
            $this->account->load('posts');
            $this->posts = $this->account->posts;

            $this->loaded = true;
        }
    }

    public function render()
    {
        return view('livewire.post-list');
    }
}
