<?php

namespace App\Utils;

class Dotenv
{
    /**
     * Add a key to the .env file
     */
    public static function addToDotEnv(string $key, mixed $value): void
    {
        $key = "$key=";

        $path = base_path('.env');
        $dotenv = file_get_contents($path);

        if (str_contains($dotenv, $key)) {
            // delete the existing token
            $dotenv = preg_replace('/'.$key.'([^\n]+)/', $key, $dotenv);
            $dotenv = preg_replace('/'.$key.'/', $key.$value, $dotenv);
            file_put_contents($path, $dotenv);
        } else {
            // add the token
            $dotenv .= "\n".$key.$value;
            file_put_contents($path, $dotenv);
        }

    }
}
