<?php

namespace App\Http\Controllers;

use Spatie\RouteAttributes\Attributes\Get;

class HomeController extends Controller
{
    #[Get('/', name: 'home', middleware: ['web'])]
    public function home()
    {
        return view('pages.home');
    }
}
