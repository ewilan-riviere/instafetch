<?php

namespace App\Services;

use App\Models\Account;
use App\Models\Post;
use App\Models\Profile;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Facebook\WebDriver\WebDriverDimension;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Panther\Client;

class InstagramService
{
    protected function __construct(
        protected string $username,
        protected string $url,
        protected string $htmlPath = 'cache/outer.html',
        protected ?DOMDocument $dom = null,
        protected ?DOMXPath $finder = null,
    ) {
    }

    /**
     * Fetch Instagram profile.
     *
     * @param  string  $username Instagram username profile to fetch.
     * @param  bool  $refresh Whether to refresh the cache or not.
     */
    public static function make(string $username, bool $refresh = false): self
    {
        $self = new self($username, "https://www.instagram.com/$username/");
        $account = Account::query()->where('key', $username)->first();
        if ($account && ! $refresh) {
            return $self;
        }

        $self->htmlPath = storage_path('app/cache/outer.html');

        $profile = Profile::first();
        if (! $profile) {
            $profile = Profile::create([
                'username' => '',
            ]);
        }

        $profile->username = $username;
        $profile->last_fetched = now();
        $profile->save();

        $self->clearMedia();
        $account = Account::first();
        if (! $account) {
            $account = new Account();
        }

        $self->extractHtml();

        $self->prepareDomDocument();
        $account = $self->parseAccount($account);
        $posts = $self->parsePosts();

        $account->key = $username;
        $account->save();
        $account->posts()->saveMany($posts);

        return $self;
    }

    public static function getProfile(): ?Profile
    {
        $profile = Profile::first();

        if (! $profile) {
            $profile = config('instagram.profile') ?? 'arcaneshow';

            if (! $profile) {
                return null;
            }

            $profile = Profile::create([
                'username' => $profile,
            ]);
        }

        if ($profile->username !== config('instagram.profile')) {
            $profile->username = config('instagram.profile');
            $profile->save();
        }

        return $profile;
    }

    /**
     * Use Panther to extract the HTML.
     */
    private function extractHtml(): string
    {
        $url = 'https://www.instagram.com/';

        if (empty(config('instagram.account.username')) || empty(config('instagram.account.password'))) {
            throw new \Exception('Instagram username or password is empty, please fill `INSTAGRAM_ACCOUNT_USERNAME` and `INSTAGRAM_ACCOUNT_PASSWORD` in `.env` file');
        }

        $client = Client::createChromeClient();
        switch (config('instagram.browser')) {
            case 'chrome':
                $client = Client::createChromeClient();
                break;

            case 'firefox':
                $client = Client::createFirefoxClient();
                break;

            case 'selenium':
                $client = Client::createSeleniumClient();
                break;

            default:
                $client = Client::createChromeClient();
                break;
        }
        $size = new WebDriverDimension(1200, 900);
        $client->manage()->window()->setSize($size);
        $crawler = $client->request('GET', $url);

        // $datr = new Cookie('datr', '', domain: '.instagram.com');
        // $dpr = new Cookie('dpr', '', domain: '.instagram.com');

        // $client->getCookieJar()->set($datr);
        // $client->getCookieJar()->set($dpr);

        $client->waitFor('#scrollview');

        $this->screenShot($client, 'cookies');

        $client->executeScript("document.getElementById('scrollview').nextSibling ? document.getElementById('scrollview').nextSibling.remove() : null");

        $this->screenShot($client, 'main');

        $form = $crawler->filter('#loginForm')->form();

        $form->setValues([
            'username' => config('instagram.account.username'),
            'password' => config('instagram.account.password'),
        ]);
        $crawler = $client->submit($form);

        $this->screenShot($client, 'logged');

        $crawler = $client->waitFor('main');

        $this->screenShot($client, 'home');

        $client->get($this->url);
        $crawler = $client->waitFor('main');

        $this->screenShot($client, 'profile');

        $outerHtml = $crawler->html();
        File::put($this->htmlPath, $outerHtml);

        $this->screenShot($client, 'saved');

        return $outerHtml;
    }

    private function screenShot(Client $client, string $name)
    {
        $client->takeScreenshot(storage_path("app/cache/screenshot-$name.png"));
    }

    /**
     * Clear all medias except `.gitignore`
     */
    private function clearMedia(): void
    {
        $path = storage_path('app/public');
        $files = File::allFiles($path);

        foreach ($files as $file) {
            if ($file->getFilename() !== '.gitignore') {
                File::delete($file->getPathname());
            }
        }

        $path = storage_path('app/cache');
        $files = File::allFiles($path);

        foreach ($files as $file) {
            if ($file->getFilename() !== '.gitignore') {
                File::delete($file->getPathname());
            }
        }
    }

    private function prepareDomDocument(): void
    {
        $this->dom = new DOMDocument();
        libxml_use_internal_errors(true);

        if (! File::exists($this->htmlPath)) {
            throw new \Exception('HTML file not found');
        }

        $this->dom->loadHTMLFile($this->htmlPath);
        $this->finder = new DOMXPath($this->dom);
    }

    /**
     * Parse the account info and fill `Account`.
     */
    private function parseAccount(Account $account): Account
    {
        $account->url = $this->url;

        $h2 = $this->dom->getElementsByTagName('h2');
        $avatars = $this->finder->query("//img[contains(@class, 'xpdipgo')]");
        $infos = $this->finder->query("//ul[contains(@class, 'x78zum5')]");

        /**
         * Extract account username
         */
        $name = $h2->count() > 0 ? $h2->item(0)->nodeValue : null;
        $account->name = trim($name);

        /**
         * Extract avatar
         */
        $avatar = null;
        // If logged in, there will be 2 images.
        if ($avatars->length > 0) {
            if ($avatars->length === 2) {
                $avatar = $avatars->item(1);
            } else {
                $avatar = $avatars->item(0);
            }

            if ($avatar instanceof DOMElement) {
                $avatar = $avatar->getAttribute('src');
                $account->avatar = $this->saveMedia($avatar);
            } else {
                $avatar = null;
            }
        }

        /**
         * Extract social values
         */
        if ($infos->length > 0) {
            /** @var DOMElement $ul */
            $ul = $infos->item(0);
            $liItems = $ul->getElementsByTagName('li');

            $current_followers = null;
            $current_following = null;
            $current_posts = null;

            if ($liItems->length > 0) {
                foreach ($liItems as $li) {
                    if ($current_followers = $this->getValue($li, 'followers')) {
                        $account->current_followers = $current_followers;
                    }

                    if ($current_following = $this->getValue($li, 'following')) {
                        $account->current_following = $current_following;
                    }

                    if ($current_posts = $this->getValue($li, 'posts')) {
                        $account->current_posts = $current_posts;
                    }
                }
            }
        }

        /**
         * Extract description
         */
        $desc = $this->finder->query("//*[contains(@class, 'x7a106z')]");
        $desc = $desc->length > 0 ? $desc->item(0)->textContent : null;
        if ($desc) {
            $desc = trim($desc);
            $desc = preg_replace('/\s+/', ' ', $desc);

            $account->description = $desc;
        }

        return $account;
    }

    /**
     * Save locally media from URL.
     */
    private function saveMedia(string $url, string $subDir = null): string
    {
        $client = new GuzzleHttpClient();
        $media = $client->get($url)->getBody()->getContents();

        $path = storage_path('app/public');
        if ($subDir) {
            $path .= "/$subDir";
        }

        if (! File::exists($path)) {
            File::makeDirectory($path, 0755, true);
        }

        $filename = uniqid().'.jpg';
        $fullPath = "$path/$filename";
        File::put($fullPath, $media);

        $relativePath = str_replace(storage_path('app/public'), '', $fullPath);

        return config('app.url').'/storage'.$relativePath;
    }

    /**
     * Extractor for social values.
     */
    private function getValue(DOMElement $value, string $key): ?string
    {
        $value = trim($value->textContent);
        $value = preg_replace('/\s+/', ' ', $value);

        if (! str_contains($value, $key)) {
            return null;
        }

        $value = explode(' ', $value);
        if (! array_key_exists(0, $value)) {
            return null;
        }

        $value = $value[0];

        return $value;
    }

    /**
     * Parse the posts and create `Post` Collection.
     *
     * @return Collection<int, Post>|null
     */
    private function parsePosts(): ?Collection
    {
        Post::query()->delete();

        $nodes = $this->finder->query("//*[contains(@class, '_aabd')]");

        if ($nodes->length < 0) {
            return null;
        }

        /** @var Collection<int, Post> */
        $images = collect([]);

        foreach ($nodes as $node) {
            $img = $this->finder->query('.//img', $node);

            if ($img->length < 0) {
                continue;
            }

            /** @var DOMElement */
            $img = $img->item(0);
            $src = $img->getAttribute('src');
            $alt = $img->getAttribute('alt');

            $media = $this->saveMedia($src, 'posts');

            $images->push(Post::create([
                'name' => $alt,
                'picture' => $media,
            ]));
        }

        return $images;
    }
}
