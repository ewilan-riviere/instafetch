<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'username',
        'last_fetched',
    ];

    protected $casts = [
        'last_fetched' => 'datetime',
    ];
}
