<?php

return [
    'browser' => 'firefox', // firefox, chrome, selenium
    'account' => [
        'username' => env('INSTAGRAM_ACCOUNT_USERNAME'),
        'password' => env('INSTAGRAM_ACCOUNT_PASSWORD'),
    ],
    'profile' => env('INSTAGRAM_PROFILE'),
];
